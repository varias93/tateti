package interfaz;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JLabel;


public class InterfazVentanaInicial extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nombreX;
	private JTextField nombreO;
	public static String jugadorX = "";
	public static String jugadorO = "";
	private JLabel lbljugadorX;
	private JLabel lbljugadorO;
	URL direccion=getClass().getResource("../imagenes/");

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					}
				catch(Exception e){}
				
				try {
					InterfazVentanaInicial frame = new InterfazVentanaInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public InterfazVentanaInicial() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(direccion.getPath()+"tateti.ico"));
		setTitle("Tateti Toroidal");
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 426, 167);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		nombreX = new JTextField();
		nombreX.setBounds(199, 22, 189, 20);
		contentPane.add(nombreX);
		nombreX.setColumns(10);
		
		nombreO = new JTextField();
		nombreO.setColumns(10);
		nombreO.setBounds(199, 53, 189, 20);
		contentPane.add(nombreO);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				    if(e.getSource() == btnAceptar){
				    	jugadorX = nombreX.getText().trim();
				    	jugadorO = nombreO.getText().trim();
				       if(jugadorX.equals("") || jugadorO.equals("") ){
				         JOptionPane.showMessageDialog(null, "Debes ingresar tu nombre.");
				       } else {
				    	 InterfazGraficaJuego tateti = new InterfazGraficaJuego(jugadorX,jugadorO);
				         tateti.setBounds(100, 100, 514, 491);
				         tateti.setLocation(700,270);
				         tateti.setVisible(true);
				         tateti.setResizable(false);				         
				         setVisible(false);
				       }
				     }
				   }
			});

		btnAceptar.setBounds(315, 94, 89, 23);
		contentPane.add(btnAceptar);
		
		lbljugadorX = new JLabel("Ingresar nombre del jugador X:");
		lbljugadorX.setBounds(25, 25, 192, 14);
		contentPane.add(lbljugadorX);
		
		lbljugadorO = new JLabel("Ingresar nombre del jugador O:");
		lbljugadorO.setBounds(25, 56, 192, 14);
		contentPane.add(lbljugadorO);
		
		
	}
}