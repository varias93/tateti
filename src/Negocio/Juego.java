package Negocio;
import java.util.Random;

public class Juego {
	private static final int[] jugadas = { 123, 456, 789, 147, 258, 369, 159, 357, 267, 249, 348, 168 };
	Tablero tablero;
	Jugador jugadorX;
	Jugador jugadorO;
	int turno;
	Random r = new Random();
	int cantidadTurnos = 0;

	public Juego() {
		tablero = new Tablero();
		jugadorX = new Jugador();
		jugadorO = new Jugador();
		this.turno = r.nextInt(2);	
	}
	
    public int jugadaGanadora() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                for (int k = 0; k < 9; k++) {
                    if (tablero.getValor(i) == tablero.getValor(j) && tablero.getValor(i) == tablero.getValor(k)) {                         
                        String res = String.valueOf(i + 1) + String.valueOf(j + 1) + String.valueOf(k + 1);
                        int resultado = Integer.parseInt(res);
                        for (int l = 0; l < 12; l++) {
                            if (jugadas[l]==resultado) {
                            return l+1;

                        }
                    }
                }
            }
        }
        
    }
        return 0;
    }

	public boolean seGano() {
		if (jugadaGanadora() != 0) {
			if (getTurno() == 1) {
				jugadorX.incrementarPuntaje();
				setTurno(r.nextInt(2));
			}
			if (getTurno() == 0) {
				jugadorO.incrementarPuntaje();
				setTurno(r.nextInt(2));
			}
			return true;
		}
		return false;
	}

	public String letraEnTurno() {
		String n;
		if (getTurno() == 0) {
			n = "X";
		} else {
			n = "O";
		}
		return n;
	}

	public void setNombres(String nombreX, String nombreO) {
		jugadorX.setNombre(nombreX);
		jugadorO.setNombre(nombreO);
	}
	public void cambiarTurno() {
		cantidadTurnos++;
		if (getTurno() == 0) {
			setTurno(1);
		} else {
			setTurno(0);
		}
	}

	public String jugadorEnTurno() {
		String jugador = (getTurno() == 0) ? jugadorX.getNombre() : jugadorO.getNombre();
		return jugador;
	}

	public void reiniciarJuego() {
		tablero.reiniciar();
		cantidadTurnos = 0;
		setTurno(r.nextInt(2));
	}

	public void reiniciarPuntaje() {
		jugadorX.setPuntaje(0);
		jugadorO.setPuntaje(0);
	}

	public int getCantidadTurnos() {
		return cantidadTurnos;
	}
	public Jugador getJugadorX() {
		return jugadorX;
	}
	public Jugador getJugadorO() {
		return jugadorO;
	}
	public Tablero getTablero() {
		return tablero;
	}
	public int getTurno() {
		return turno;
	}
	public void setTurno(int turno) {
		this.turno = turno;
	}
}